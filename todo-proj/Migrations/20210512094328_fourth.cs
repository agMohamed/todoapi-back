﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApi.Migrations
{
    public partial class fourth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Employees_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DeptCode",
                table: "Employees",
                column: "DeptCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DeptCode",
                table: "Employees",
                column: "DeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DeptCode",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Employees_DeptCode",
                table: "Employees");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentDeptCode",
                table: "Employees",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
